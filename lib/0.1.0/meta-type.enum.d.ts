export declare enum AuthfMetaTypeEnum {
    AUTHENTICATION_TOKEN_PAIR = "cryptexlabs.authf.authentication.token-pair",
    AUTHENTICATION_VERIFICATION = "cryptexlabs.authf.authentication.verification"
}
