export interface SamsungLegacyAuthenticateDataInterface {
    accessToken: string;
    apiServerUrl: string;
}
