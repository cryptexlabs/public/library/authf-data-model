import { AuthenticationProviderInterface } from "../../authentication-provider.interface";
import { AuthenticationProviderTypeEnum } from "../../authentication-provider-type.enum";
import { SamsungLegacyAuthenticationProviderDataInterface } from "./samsung-legacy.authentication-provider.data.interface";
export interface SamsungLegacyAuthenticationProviderInterface extends AuthenticationProviderInterface {
    data: SamsungLegacyAuthenticationProviderDataInterface;
    type: AuthenticationProviderTypeEnum.SAMSUNG_LEGACY;
}
