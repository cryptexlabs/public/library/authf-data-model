export interface ApiAuthAuthenticateDataInterface {
    apiKey: string;
    secret: string;
}
