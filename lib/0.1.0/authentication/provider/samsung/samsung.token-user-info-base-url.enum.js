"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SamsungTokenUserInfoBaseUrlEnum = void 0;
var SamsungTokenUserInfoBaseUrlEnum;
(function (SamsungTokenUserInfoBaseUrlEnum) {
    SamsungTokenUserInfoBaseUrlEnum["EU"] = "https://eu-auth2.samsungosp.com";
    SamsungTokenUserInfoBaseUrlEnum["EU_STAGE"] = "https://stg-eu-auth2.samsungosp.com";
    SamsungTokenUserInfoBaseUrlEnum["US"] = "https://us-auth2.samsungosp.com";
    SamsungTokenUserInfoBaseUrlEnum["US_STAGE"] = "https://stg-us-auth2.samsungosp.com";
    SamsungTokenUserInfoBaseUrlEnum["AP"] = "https://ap-auth2.samsungosp.com";
    SamsungTokenUserInfoBaseUrlEnum["AP_STAGE"] = "https://stg-ap-auth2.samsungosp.com";
    SamsungTokenUserInfoBaseUrlEnum["CN"] = "https://cn-auth2.samsungosp.com.cn";
    SamsungTokenUserInfoBaseUrlEnum["CN_STAGE"] = "https://stg-cn-auth2.samsungosp.com.cn";
})(SamsungTokenUserInfoBaseUrlEnum = exports.SamsungTokenUserInfoBaseUrlEnum || (exports.SamsungTokenUserInfoBaseUrlEnum = {}));
