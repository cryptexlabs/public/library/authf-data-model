export interface BasicAuthAuthenticationProviderDataInterface {
    username: string;
    password: string;
}
