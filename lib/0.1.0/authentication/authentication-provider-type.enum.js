"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthenticationProviderTypeEnum = void 0;
var AuthenticationProviderTypeEnum;
(function (AuthenticationProviderTypeEnum) {
    AuthenticationProviderTypeEnum["BASIC"] = "basic";
    AuthenticationProviderTypeEnum["API"] = "api";
    AuthenticationProviderTypeEnum["SAMSUNG_LEGACY"] = "samsung-legacy";
    AuthenticationProviderTypeEnum["SAMSUNG"] = "samsung";
    AuthenticationProviderTypeEnum["REAUTH"] = "reauth";
})(AuthenticationProviderTypeEnum = exports.AuthenticationProviderTypeEnum || (exports.AuthenticationProviderTypeEnum = {}));
