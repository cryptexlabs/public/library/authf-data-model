import { RefreshAuthenticateDataInterface } from "./refresh.authenticate.data.interface";
import { AuthenticateTypeEnum } from "../authenticate-type.enum";
export interface RefreshAuthenticateInterface {
    data: RefreshAuthenticateDataInterface;
    type: AuthenticateTypeEnum.REFRESH;
}
