export declare enum AuthenticationProviderTypeEnum {
    BASIC = "basic",
    API = "api",
    SAMSUNG_LEGACY = "samsung-legacy",
    SAMSUNG = "samsung",
    REAUTH = "reauth"
}
