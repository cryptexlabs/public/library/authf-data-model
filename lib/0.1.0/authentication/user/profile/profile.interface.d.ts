import { EmailInterface } from "./email.interface";
import { PhotoInterface } from "./photo.interface";
import { NameInterface } from "./name.interface";
import { PhoneInterface } from "./phone.interface";
export interface ProfileInterface {
    email: {
        primary: EmailInterface | null;
        other: EmailInterface[];
    };
    phone: {
        primary: PhoneInterface | null;
        other: PhoneInterface[];
    };
    photo: {
        primary: PhotoInterface | null;
        other: PhotoInterface[];
    };
    name: NameInterface;
}
