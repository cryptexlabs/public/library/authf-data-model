"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PhoneTypeEnum = void 0;
var PhoneTypeEnum;
(function (PhoneTypeEnum) {
    PhoneTypeEnum["WORK"] = "work";
    PhoneTypeEnum["PERSONAL"] = "home";
    PhoneTypeEnum["CELL"] = "cell";
    PhoneTypeEnum["UNKNOWN"] = "unknown";
})(PhoneTypeEnum = exports.PhoneTypeEnum || (exports.PhoneTypeEnum = {}));
