export declare enum PhoneTypeEnum {
    WORK = "work",
    PERSONAL = "home",
    CELL = "cell",
    UNKNOWN = "unknown"
}
