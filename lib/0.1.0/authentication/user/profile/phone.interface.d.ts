import { PhoneTypeEnum } from "./phone.type.enum";
export interface PhoneInterface {
    value: string;
    type: PhoneTypeEnum;
}
