export declare enum EmailTypeEnum {
    WORK = "work",
    PERSONAL = "personal",
    UNKNOWN = "unknown"
}
