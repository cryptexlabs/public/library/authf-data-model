import { UserInterface } from "../user";
export interface ReAuthAuthorizeDataInterface {
    accessToken: string;
    user: UserInterface;
    authenticationId: string;
    redirectUrl: string;
    finalRedirectUrl: string;
}
