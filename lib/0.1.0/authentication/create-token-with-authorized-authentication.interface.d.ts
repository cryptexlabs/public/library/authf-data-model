import { AuthenticateTypes } from "./authentication.types";
import { AuthenticationInterface } from "./authentication.interface";
export interface CreateTokenWithAuthorizedAuthenticationInterface {
    authenticate: AuthenticateTypes;
    authentication: AuthenticationInterface;
}
