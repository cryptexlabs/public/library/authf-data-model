import { TokenInterface } from "./token.interface";
export interface TokenPairInterface {
    access: TokenInterface;
    refresh: TokenInterface;
}
