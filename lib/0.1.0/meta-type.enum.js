"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthfMetaTypeEnum = void 0;
var AuthfMetaTypeEnum;
(function (AuthfMetaTypeEnum) {
    AuthfMetaTypeEnum["AUTHENTICATION_TOKEN_PAIR"] = "cryptexlabs.authf.authentication.token-pair";
    AuthfMetaTypeEnum["AUTHENTICATION_VERIFICATION"] = "cryptexlabs.authf.authentication.verification";
})(AuthfMetaTypeEnum = exports.AuthfMetaTypeEnum || (exports.AuthfMetaTypeEnum = {}));
