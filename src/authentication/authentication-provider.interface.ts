import { AuthenticationProviderTypeEnum } from "./authentication-provider-type.enum";

export interface AuthenticationProviderInterface {
  data: any;
  type: AuthenticationProviderTypeEnum;
}
