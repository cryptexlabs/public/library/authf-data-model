import { SamsungTokenUserInfoBaseUrlEnum } from "./samsung.token-user-info-base-url.enum";

export interface SamsungAnyAuthenticateDataInterface {
  accessToken: string;
  clientId: string;
  userId: string;
  authBaseUrl?: string;
  apiBaseUrl?: SamsungTokenUserInfoBaseUrlEnum;
}
