import { AuthenticationProviderInterface } from "../../authentication-provider.interface";
import { AuthenticationProviderTypeEnum } from "../../authentication-provider-type.enum";
import { SamsungAuthenticationProviderDataInterface } from "./samsung.authentication-provider.data.interface";

export interface SamsungAuthenticationProviderInterface
  extends AuthenticationProviderInterface {
  data: SamsungAuthenticationProviderDataInterface;
  type: AuthenticationProviderTypeEnum.SAMSUNG;
}
