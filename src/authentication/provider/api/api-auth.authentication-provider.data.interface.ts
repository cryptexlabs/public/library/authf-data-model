export interface ApiAuthAuthenticationProviderDataInterface {
  apiKey: string;
  secret: string;
}
