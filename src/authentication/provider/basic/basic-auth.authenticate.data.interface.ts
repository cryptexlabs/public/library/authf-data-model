export interface BasicAuthAuthenticateDataInterface {
  username: string;
  password: string;
}
