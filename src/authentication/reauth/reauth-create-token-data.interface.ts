import { UserInterface } from "../user";

export interface ReAuthCreateTokenDataInterface {
  accessToken: string;
  user: UserInterface;
  userId: string;
  extra?: any;
  finalRedirectUrl: string;
}
