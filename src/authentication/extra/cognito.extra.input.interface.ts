export interface CognitoExtraInputInterface {
  poolId: string;
  developerProviderName: string;
  region: string;
  label: string;
}
