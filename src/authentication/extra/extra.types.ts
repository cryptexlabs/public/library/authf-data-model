import { CognitoExtraInputInterface } from "./cognito.extra.input.interface";
import { CognitoExtraResultInterface } from "./cognito.extra.result.interface";

export type ExtraInputType = CognitoExtraInputInterface[];
export type ExtraResultType = CognitoExtraResultInterface[];
