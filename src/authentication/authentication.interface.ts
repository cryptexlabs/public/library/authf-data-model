import { AuthenticationTokenDataInterface } from "./token";
import { AuthenticationTypes } from "./authentication.types";
import { ExtraInputType, ExtraTypeEnum } from "./extra";

export interface AuthenticationInterface {
  token: AuthenticationTokenDataInterface;
  providers: AuthenticationTypes[];
  extra?: Record<ExtraTypeEnum, ExtraInputType>;
}
